#ifndef _MEM_INTERNALS_
#define _MEM_INTERNALS_

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>

#define REGION_MIN_SIZE (2 * 4096)      // минимальный размер региона

struct region {
    void* addr;         // указатель на начало региона?
    size_t size;        // размер региона
    bool extends;       //  расширять? (скорее всего типо ест)
};
static const struct region REGION_INVALID = {0};

inline bool region_is_invalid( const struct region* r ) {
    return r->addr == NULL;
}

typedef struct { size_t bytes; } block_capacity;    // вместимость блока (св сколько туда можно положить памяти)
typedef struct { size_t bytes; } block_size;        // размер блока (просто размер всего блока)
// Размер блока всегда на offsetof(struct block_header, contents) больше, чем его вместимость.)

/*
 * Структура заголовка блока
 * (куча задаётся ссылкой на заголовок первого блока)
 * */
struct block_header {
  struct block_header*    next;     // ссылка на следующий блок
  block_capacity capacity;          // размер блока (сколько можно положить туда)
  bool           is_free;           // свободен блок или занят
  uint8_t        contents[];        // гибкий элемент массива (содержание???)
};

/* получение размера блока по его вместимости */
inline block_size size_from_capacity( block_capacity cap ) {
    return (block_size) {cap.bytes + offsetof( struct block_header, contents ) };
}

/* получение вместимости по его размеру */
inline block_capacity capacity_from_size( block_size sz ) { return (block_capacity) {sz.bytes - offsetof( struct block_header, contents ) }; }



#endif
