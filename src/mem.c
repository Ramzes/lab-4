#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );


/* достаточно ли большой блок
 * query - размер запрашиваемой памяти в байтах
 * true - всё ок, вместимости блока хватит
 * false - вместимости блока не хватит
 * */
static bool block_is_big_enough( size_t query, struct block_header* block ) {
    return block->capacity.bytes >= query;
}

/* кол-во страниц
 * getpagesize() - Возвращает количество байтов на странице
 * ((mem % getpagesize()) > 0) нужна для того, что бы учитывать неполные страницы
 * */
static size_t pages_count( size_t mem ) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

/* размер всех страниц в байтах */
static size_t round_pages( size_t mem ) {
    return getpagesize() * pages_count( mem ) ;
}


/*
 * инициализация блока
 * */
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}



/* возвращаем размер региона
 * Случай 1 - возвращаем кол-во в байтах, нужное для того что бы вместить данные
 * Случай 2 - если кол-во байт которое нам нужно меньше чем мин размер, то возвразаем минимальный размер для региона
 * */
static size_t region_actual_size( size_t query ) {
    return size_max( round_pages( query ), REGION_MIN_SIZE );
}



/* эту функцию берём из mem_internals.h
 * true - регион НЕ валидный
 * false - регион валидный
 * То есть указывает ли указатель на первые заголовок блока в регионе
 * */
extern inline bool region_is_invalid( const struct region* r );



/* Входные параметры mmap
 * addr - Желаемый адрес начала участка отображённой памяти, если хотим что бы ядро само решило че и куда делать, то - 0
 * len - Кол-во байт, которые нужно отобразить в память
 * prot - число, определяющее степень защищённости отображенного участка памяти
 * flags - описывает атрибуты области ...
 * filedes - дескриптор файла, который нужно отобразить
 * offset - смещение отображенного участка от начала файла
 * инфа на счёт флагов (additional_flags)
 * MAP_FIXED - Не использовать другой адрес, если адрес задан в параметрах функции.
 * Если заданный адрес не может быть использован, то функция mmap вернет сообщение об ошибке.
 * MAP_FIXED_NOREPLACE - похожий на map fixed
 * */
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}


/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region( void const * addr, size_t query ) {

    block_capacity reg_cap = {.bytes = query};
    block_size reg_size = size_from_capacity(reg_cap);
    size_t act_reg_size = region_actual_size(reg_size.bytes);


    void * ptr_region = map_pages(addr, act_reg_size, MAP_FIXED_NOREPLACE);

    if (ptr_region == MAP_FAILED) {
        ptr_region = map_pages(addr, act_reg_size, 0);
    }
    if (ptr_region == MAP_FAILED) {
        return REGION_INVALID;
    }

    block_init(ptr_region, (block_size) {.bytes = act_reg_size}, NULL);

    struct region new_reg = {.addr = ptr_region, .size = act_reg_size, .extends = ptr_region == addr};
    return new_reg;

}

/* возвращает указатель на адрес после блока */
static void* block_after( struct block_header const* block );


/* инициализация кучи */
void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

/* Требуеся ли разделять блок?
 * true - надо
 * false - не надо
 * */
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/* разделяем слишком, если большой блок
 * на входе блок, который исследуем
 * */
static bool split_if_too_big( struct block_header* block, size_t query ) {

    if (block_splittable(block, query)) {
        size_t new_size = size_max(query, BLOCK_MIN_CAPACITY);      // чекаем не слишком ли мало нам надо выделить

        block_capacity old_capacity = block->capacity;
        size_t old_size_block = size_from_capacity(old_capacity).bytes;                                 // текущий размер большого блока

        block_capacity new_capacity = (block_capacity) {.bytes = new_size};
        size_t new_size_block = size_from_capacity(new_capacity).bytes;                     //  новый размер нужного нам (уже не) большого блока

        size_t size_for_second_block = old_size_block - new_size_block;
        block_size init_size_second_block = (block_size) {.bytes = size_for_second_block};                  // размер блока для остаточного блока

        block->capacity = (block_capacity) {.bytes = new_size};                                         // ставим новую вместимость нужного блока

        void* ptr_second_block = block_after(block);                                                            // ссылка на следующий блок (второй)

        block_init(ptr_second_block, init_size_second_block, block->next);                   // инициализируем второй блок

        block->next = (struct block_header*) ptr_second_block;                                                 // в текущем блоке меняем адрес на следующий на тот, который мы только что инициализировали

        return true;
    }
    return false;

}


/*  --- Слияние соседних свободных блоков --- */

/* возвращает указатель на адрес после блока */
static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

/* Можно ли сделать слияние блоков
 * true - можно
 * false - нельзя
 * */
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

/* Пытаемся соединить соседние блоки */
static bool try_merge_with_next( struct block_header* block ) {
    if (block != NULL && block-> next != NULL && mergeable(block, block->next)) {
        size_t size_next_block = size_from_capacity(block->next->capacity).bytes;           // размер следующего блока
        block->capacity.bytes = block->capacity.bytes + size_next_block;                        // подумать верно ли тут присвивается размер в capacity
        block->next = block->next->next;                                                        // меняем указатель на следующий блок
        return true;
    }
    return false;

}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


/*  ищем хороший блок, если его нету, то возвращаем последний блок*/
static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    struct block_header* now_block = block;
    while(now_block) {
        while(try_merge_with_next(now_block));                                                                    // делаем слияние блоков на сколько это возможно
        if (now_block->is_free && block_is_big_enough(sz, now_block)) {
            struct block_search_result result = {.type = BSR_FOUND_GOOD_BLOCK,.block = now_block };
            return result;                                                                                              // возвращаем найденый хороший блок
        }
        if (now_block->next) {
            now_block = now_block->next;
        }
        else {
            break;
        }
    }
    struct block_search_result result = {.type = BSR_REACHED_END_NOT_FOUND, now_block};
    return result;

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result block_sear = find_good_or_last(block, query);        // смотрим есть ли в куче хороший блок
    if (block_sear.type == BSR_FOUND_GOOD_BLOCK) {                                      // если есть, то возвращаем его
        split_if_too_big(block_sear.block, query);                                   // иначе попытка разделить последний блок ?
        block_sear.block->is_free = false;                                                 // говорим что он теперь занят .. |
    }
    return block_sear;
}


/* увеличение кучи */
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    const struct region o_reg = alloc_region(block_after(last), query);               // алоцируем память под регион
    if (region_is_invalid(&o_reg)){
        return NULL;                                               // проверяем на удачную аллокацию
    }
    last->next = o_reg.addr;                                                                    // next последнего блока делаем в указатель на начало новой области кучи
    if (try_merge_with_next(last)) {
        return last;                                         // пробуем соеденить блоки, если все ок то возвращаем посдени блок
    }
    struct block_header * result = last-> next;
    return result;                                                                        // если нет то явно возвращаем указатель на блок в начале нового региона
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    struct block_search_result b_ser = try_memalloc_existing(query, heap_start);       // Пробуем выделить память в куче, не расширяя её
    if (b_ser.type == BSR_REACHED_END_NOT_FOUND) {                                            // если без расширения не обойтись, то
        grow_heap(b_ser.block, query);                                                  // увеличиваем кучу
        b_ser = try_memalloc_existing(query, heap_start);                              // Пробуем еще раз выделить память в куче
        if (b_ser.type != BSR_FOUND_GOOD_BLOCK){
            return NULL;                                 // если опять не получилось то ошибка
        }
    }
    return b_ser.block;                                                                      // возвращаем блок
}

/* основа
 * query - запрашиваемая память в байтах */
void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
